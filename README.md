﻿# Gewässerkarte Karlsruhe

Ziel der Gewässerkarte Karlsruhe ist es für Angler aus dem Raum Karlsruhe eine Übersicht über die vorhanden Gewässer sowie deren Pachtverhältnisse zu erhalten.

Hierbei sollen lediglich die Gewässer selbst verzeichnet werden, sowie allgemein interessante Informatione wie Bootsplätze. Spezielle Infos zu den Gewässern wie  zeitliche Ausnahmeregelungen und Befischungsverbote innerhalb der Gewässer sollen nicht erfasst werden. Hierzu soll auf den jeweiligen Pächter (Verein) und dessen Webseite verwiesen werden.

Zu finden ist die Gewässerkarte [hier](http://www.pieceofsoul.de/gewaesserkarte-karlsruhe/)

# Credits

Die Gewässerkarte wurde unter der Zuhilfenahme folgender Daten, Dienste und
Werkzeuge erstellt:

## Gewässerdaten

[https://www.lubw.baden-wuerttemberg.de/](https://www.lubw.baden-wuerttemberg.de/)

Grundlage: Daten aus dem Räumlichen Informations- und Planungssystem (RIPS) der Landesanstalt für Umwelt, Messungen und Naturschutz Baden-Württemberg (LUBW).

## Map Icons

[http://mapicons.nicolasmollet.com/](http://mapicons.nicolasmollet.com/)

Map Icons Collection, Author : Nicolas Mollet

## Google Fonts

[https://www.google.com/fonts](https://www.google.com/fonts)

## Leaflet

[http://leafletjs.com/](http://leafletjs.com/)

An Open-Source JavaScript Library for Mobile-Friendly Interactive Maps

### Leaflet plugins

[https://github.com/shramov/leaflet-plugins](https://github.com/shramov/leaflet-plugins)

Miscellaneous plugins for Leaflet library for services that need to display route information and need satellite imagery from different providers.

## OpenStreetMap

[http://www.openstreetmap.de/](http://www.openstreetmap.de/)

## QGIS 

[http://www.qgis.org/de/site/](http://www.qgis.org/de/site/)

Ein freies Open-Source-Geographisches-Informationssystem

## geojson.io

[http://geojson.io/](http://geojson.io/)

Online GEOJSON Editor
